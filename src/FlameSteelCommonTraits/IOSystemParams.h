/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   FSEGTIOSystemParams.h
 * Author: demensdeum
 *
 * Created on March 8, 2017, 1:29 PM
 */

#ifndef IOSYSTEMPARAMS_H
#define IOSYSTEMPARAMS_H

#include <memory>

using namespace std;

class IOSystemParams {
public:
    IOSystemParams() {};

    shared_ptr<string> title;

    int width = 1024;
    int height = 768;
    bool windowed = true;

    virtual ~IOSystemParams() {};

};

#endif

